package com.example.ex1.ui.calcolatrice;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.ex1.R;
import com.example.ex1.databinding.FragmentCalcolatriceBinding;

public class CalcolatriceFragment extends Fragment {
    private FragmentCalcolatriceBinding layout;
    float cache = 0;
    TextView display;
    Button bt7;
    Button bt8;
    Button bt9;
    Button bt4;
    Button bt5;
    Button bt6;
    Button bt1;
    Button bt2;
    Button bt3;
    Button bt0;

    Button divisione;
    Button moltiplicazione;
    Button sottrazione;
    Button somma;
    Button risultato;
    Button clean;
    Button operazione;
    Boolean pressOp = true;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        layout = FragmentCalcolatriceBinding.inflate(inflater, container, false);

        display = layout.display;

        bt7 = layout.bt7;
        bt8 = layout.bt8;
        bt9 = layout.bt9;
        bt4 = layout.bt4;
        bt5 = layout.bt5;
        bt6 = layout.bt6;
        bt1 = layout.bt1;
        bt2 = layout.bt2;
        bt3 = layout.bt3;
        bt0 = layout.bt0;

        divisione = layout.btDiv;
        moltiplicazione = layout.btMolt;
        sottrazione = layout.btSott;
        somma = layout.btSomma;
        risultato = layout.btUguale;
        clean = layout.btClean;

        initBtNumber();
        initBtOperation();

        return layout.getRoot();
    }

    void initBtNumber() {
        bt7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                elaborazione(view);
            }
        });

        bt8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                elaborazione(view);
            }
        });

        bt9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                elaborazione(view);
            }
        });

        bt4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                elaborazione(view);
            }
        });

        bt5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                elaborazione(view);
            }
        });

        bt6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                elaborazione(view);
            }
        });

        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                elaborazione(view);
            }
        });

        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                elaborazione(view);
            }
        });

        bt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                elaborazione(view);
            }
        });

        bt0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                elaborazione(view);
            }
        });
    }

    void initBtOperation() {
        divisione.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                elaborazione(view);
            }
        });

        moltiplicazione.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                elaborazione(view);
            }
        });

        sottrazione.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                elaborazione(view);
            }
        });

        somma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                elaborazione(view);
            }
        });

        clean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                elaborazione(view);
            }
        });

        risultato.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                elaborazione(view);
            }
        });
    }

    String cleanValue(Float val) {
        if (("" + val).endsWith(".0")) {
            return ("" + val).replace(".0", "");
        } else {
            return "" + val;
        }
    }

    void elaborazione(View value) {
        Button tempB = (Button) value;
        if (tempB.getTag() != null && tempB.getTag().toString().equals("op")) {
            pressOp = true;
            switch (tempB.getText().toString()) {
                case "CE":
                    if (operazione != null) {
                        operazione.setBackgroundColor(getResources().getColor(R.color.gray));
                    }
                    display.setText("0");
                    operazione = null;
                    cache = 0;
                    break;
                case "=":
                    float risultato = 0;
                    switch (operazione.getText().toString().toLowerCase()) {
                        case "+":
                            risultato = cache + Float.parseFloat(display.getText().toString());
                            break;
                        case "-":
                            risultato = cache - Float.parseFloat(display.getText().toString());
                            break;
                        case "/":
                            risultato = cache / Float.parseFloat(display.getText().toString());
                            break;
                        case "x":
                            risultato = cache * Float.parseFloat(display.getText().toString());
                            break;
                    }

                    if (operazione != null) {
                        operazione.setBackgroundColor(getResources().getColor(R.color.gray));
                    }

                    display.setText(cleanValue(risultato));

                    cache = Float.parseFloat(display.getText().toString());
                    operazione = null;
                    break;
                default:
                    if (operazione != null) {
                        operazione.setBackgroundColor(getResources().getColor(R.color.gray));
                    }
                    tempB.setBackgroundColor(getResources().getColor(R.color.superred));
                    operazione = tempB;
                    break;
            }

            Log.i("Click", "E' stato premuto il tasto per operazione:" + tempB.getText().toString());
        } else {
            if (pressOp) {
                display.setText(tempB.getText().toString());
                pressOp = false;
            } else {
                Float val = Float.parseFloat(display.getText().toString()) * 10 + Float.parseFloat(tempB.getText().toString());
                display.setText(cleanValue(val));
            }

            if (operazione == null) {
                cache = Float.parseFloat(display.getText().toString());
            }

            Log.i("Click", "E' stato premuto il tasto con numero:" + tempB.getText().toString());
        }
    }
}
